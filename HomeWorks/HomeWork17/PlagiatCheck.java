public class PlagiatCheck {

    private String[] elements;
    private String text;
    private int[] count;
    private int[] vector;
    private int percent;

    public PlagiatCheck(String s) {
        this.elements = new String[10];
        this.count = new int[10];
        this.text = s;
        this.percent = 0;
    }

    public void editingOfLength() {
        this.elements = new String[countOfSpace() + 1];
        this.count = new int[countOfSpace() + 1];
        stringToStringArray();
    }

    public void stringToStringArray() {
        char[] c = toLower();
        this.text = "";
        int j = 0;
        for (int i = 0; i < c.length ; i++) {
            if (c[i] != ' ') {
                if (c[i] > 64) {
                    this.text += c[i];
                }
                if (i == c.length - 1) {
                    this.elements[j] = this.text;
                }
            } else {
                this.elements[j] = this.text;
                j++;
                this.text = "";
            }
        }
    }

    public char[] toLower() {
        char[] c = this.text.toCharArray();
        int left = 'А';
        int right = 'Я';
        for (int i = 0; i < c.length; i++) {
            if (c[i] >= left && c[i] <= right) {
                c[i] = (char) (c[i] + 32);
            }
        }
        return c;
    }

    public int countOfSpace() {
        char[] c = this.text.toCharArray();
        int count = 0;
        for (int i = 0; i < c.length; i++) {
            if (c[i] == ' ') {
                count++;
            }
        }
        return count;
    }

    public void print() {
        for (int i = 0; i < this.elements.length; i++) {
            System.out.print(this.elements[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < this.vector.length; i++) {
            System.out.print(this.vector[i] + " ");
        }
    }

    public void sort() {
        String s;
        for (int i = this.elements.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (compareStrings(this.elements[j + 1], this.elements[j])) {
                    s = this.elements[j];
                    this.elements[j] = this.elements[j + 1];
                    this.elements[j + 1] = s;
                }
            }
        }
    }


    public boolean compareStrings(String s1, String s2) {
        char[] c1 = s1.toCharArray();
        char[] c2 = s2.toCharArray();
        for (int i = 0; i < Math.min(c1.length, c2.length); i++) {
            if (c1[i] > c2[i]) {
                return false;
            } else if (c1[i] < c2[i]) {
                return true;
            } else if (i == Math.min(c1.length, c2.length)) {
                if (c1.length < c2.length) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        return true;
    }

    public void countOfElements() {
        for (int i = 0; i < this.count.length; i++) {
            this.count[i] = 1;
        }
        for (int i = 0; i < this.elements.length - 1; i++) {
            while(this.elements[i].equals(this.elements[i + 1])) {
                this.count[i]++;
                deleteDublicates(i + 1);
                if (i == this.elements.length - 1) {
                    break;
                }
            }
        }
    }

    public void deleteDublicates(int index) {
        for (int i = index; i < this.elements.length - 1; i++) {
            this.elements[i] = this.elements[i + 1];
            this.count[i] = this.count[i + 1];
        }
        String[] newElements = this.elements;
        int[] newCount = this.count;
        this.elements = new String[this.elements.length - 1];
        this.count = new int[this.count.length - 1];
        for (int i = 0; i < this.count.length; i++) {
            count[i] = 1;
        }
        for (int i = 0; i < this.count.length; i++) {
            this.elements[i] = newElements[i];
            this.count[i] = newCount[i];
        }
    }

    public void mergeSort(PlagiatCheck other1, PlagiatCheck other2) {
        this.count = new int[other1.count.length + other2.count.length];
        this.elements = new String[other1.count.length + other2.count.length];
        int i=0, j=0;
        for (int k=0; k<this.count.length; k++) {
            if (i > other1.count.length-1) {
                int a = other2.count[j];
                this.count[k] = a;
                this.elements[k] = other2.elements[j];
                j++;
            }
            else if (j > other2.count.length-1) {
                int a = other1.count[i];
                this.count[k] = a;
                this.elements[k] = other1.elements[i];
                i++;
            }
            else if (compareStrings(other1.elements[i], other2.elements[j])) {
                int a = other1.count[i];
                this.count[k] = a;
                this.elements[k] = other1.elements[i];
                i++;
            }
            else {
                int b = other2.count[j];
                this.count[k] = b;
                this.elements[k] = other2.elements[j];
                j++;
            }
        }

    }

    public void makeVector(PlagiatCheck other) {
        this.vector = new int[other.elements.length];
        for (int i = 0; i < this.vector.length; i++) {
            for (int j = 0; j < this.elements.length; j++) {
                if (this.elements[j].equals(other.elements[i])) {
                    this.vector[i] = this.count[j];
                    break;
                } else {
                    this.vector[i] = 0;
                }
            }
        }
    }

    public void check(PlagiatCheck other) {
        int numerator = 0;
        int denominator1 = 0;
        int denominator2 = 0;
        for (int i = 0; i < this.vector.length; i++) {
            numerator += this.vector[i] * other.vector[i];
        }
        for (int i = 0; i < this.vector.length; i++) {
            denominator1 += this.vector[i] * this.vector[i];
        }
        for (int i = 0; i < other.vector.length; i++) {
            denominator2 += other.vector[i] * other.vector[i];
        }
        this.percent = (int)(numerator/(Math.sqrt(denominator1) * Math.sqrt(denominator2)) * 100);
    }

    public void printPercent() {
        System.out.println(this.percent + " %");
    }

}
