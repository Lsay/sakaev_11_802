class program {
    public static int parse(char number[]) {
        int x = 0;
        int koeff = 1;
        for (int i = number.length - 1; i >= 0; i--) {
            x += (number[i] - '0') * koeff;
            koeff *= 10;
        }
        return x;
    }

    public static void main(String[] args) {
        char numberAsArray[] = {'1', '2', '3'};
        int num;
        num = parse(numberAsArray);
        System.out.println(num);
    }
}