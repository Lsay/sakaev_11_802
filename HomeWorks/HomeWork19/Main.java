import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        InputStream inputStream = new FileInputStream("int.txt");
        MyScanner myScanner = new MyScanner();
        int number1 = 0;
        number1 = myScanner.nextInt(inputStream);
        System.out.println(number1);
        double number2 = 0.0;
        InputStream inputStream1 = new FileInputStream("double.txt");
        number2 = myScanner.nextDouble(inputStream1);
        System.out.println(number2);
        InputStream inputStream2 = new FileInputStream("String.txt");
        String string = "";
        string = myScanner.nextString(inputStream2);
        System.out.println(string);
    }
}
