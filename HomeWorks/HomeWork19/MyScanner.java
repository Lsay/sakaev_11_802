import java.io.FileInputStream;
import java.io.InputStream;

public class MyScanner {

    public int nextInt(InputStream inputStream) throws Exception {
        boolean minus = false;
        int num = 0;
        int value = inputStream.read();
        if (value == (int)'-') {
            minus = true;
            value = inputStream.read();
        }
        while (value > 32) {
            num = num * 10 + (value - '0');
            value = inputStream.read();
        }
        if (minus) {
            return -1 * num;
        } else {
            return num;
        }
    }

    public double nextDouble(InputStream inputStream) throws Exception{
        boolean minus = false;
        double num = 0;
        boolean dot = false;
        int powOfTen = 1;
        int value = inputStream.read();
        if (value == (int)'-') {
            minus = true;
            value = inputStream.read();
        }
        while (value > 31) {
            if (value == (int)'.') {
                dot = true;
                value = inputStream.read();
            }
            if (dot) {
                powOfTen = powOfTen * 10;
                num += (value - '0')/(double)powOfTen;
            } else {
                num = num * 10 + (value - '0');
            }
            value = inputStream.read();
        }
        if (minus) {
            return -1 * num;
        } else {
            return num;
        }
    }

    public String nextString(InputStream inputStream) throws Exception{
        int value = 0;
        String string = "";
        value = inputStream.read();
        while (value != ' ' && value != -1 && value != '\0') {
            string += (char)value;
            value = inputStream.read();
        }
        return string;
    }
}
