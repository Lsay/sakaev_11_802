public class Human {
    int age;
    String name;
    int height;

    public Human(int age, String name) {

        this.age = age;
        this.name = name;
    }

    public Human(int age) {
        this.age = age;
    }

    public Human() {

    }
}
