public class Rhomb extends Figures{
    private double height;
    private double base;
    public Rhomb(double x, double y, String name, double base, double height) {
        super(x, y, name);
        this.height = height;
        this.base = base;
    }

    @Override
    public void calculateArea() {
        this.area = this.base * this.height / 2;
    }

    @Override
    public void calculatePerimetr() {
        this.perimetr = 4 * this.base;
    }

    @Override
    public void printArea() {
        System.out.println(this.name + " " + this.area);
    }

    @Override
    public void printPerimetr() {
        System.out.println(this.name +" " + this.perimetr);
    }

    @Override
    public void printCentre() {
        System.out.println(this.name + " " + this.x + " " + this.y);
    }
}
