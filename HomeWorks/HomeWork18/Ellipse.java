public class Ellipse extends Figures {
    private static final double PI = 3.14;
    private double bigRadius;
    private double smallRadius;

    public Ellipse (double x, double y, String name, double smallRadius, double bigRadius) {
        super(x, y, name);
        this.bigRadius = bigRadius;
        this.smallRadius = smallRadius;
    }

    @Override
    public void calculateArea() {
        this.area = PI * this.smallRadius * this.bigRadius;
    }

    @Override
    public void calculatePerimetr() {
        this.perimetr = 2 *  PI * (this.smallRadius + this.bigRadius);
    }

    @Override
    public void printArea() {
        System.out.println(this.name + " " + this.area);
    }

    public double getBigRadius() {
        return bigRadius;
    }

    public double getSmallRadius() {
        return smallRadius;
    }

    @Override
    public void printPerimetr() {
        System.out.println(this.name +" " + this.perimetr);
    }

    @Override
    public void printCentre() {
        System.out.println(this.name + " " + this.x + " " + this.y);
    }
}
