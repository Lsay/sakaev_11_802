public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(2, 5, "circle", 3);
        Ellipse ellipse = new Ellipse(3, 4, "ellipse", 2, 4);
        Restangle restangle = new Restangle(7, 9, "restangle", 2.1, 3);
        Square square = new Square(3, 3, "square", 5);
        Rhomb rhomb = new Rhomb(2, 2, "rhomb", 5, 7);
        Trapezium trapezium = new Trapezium(1, 0, "trapexzium", 5, 5, 5, 3, 7);
        Figures figures[] = {circle, ellipse, restangle, square, rhomb, trapezium};
        for (int i = 0; i < figures.length; i++) {
            figures[i].calculateArea();
        }
        for (int i = 0; i < figures.length; i++) {
            figures[i].calculatePerimetr();
        }
        System.out.println("Areas");
        System.out.println();
        for (int i = 0; i < figures.length; i++) {
            figures[i].printArea();
        }
        System.out.println();
        System.out.println("Perimetrs");
        System.out.println();
        for (int i = 0; i < figures.length; i++) {
            figures[i].printPerimetr();
        }
        System.out.println();
        System.out.println("Centres");
        System.out.println();
        for (int i = 0; i < figures.length; i++) {
            figures[i].printCentre();
        }
    }
}
