public class Main {
    public static int mainSum = 0;

    public static int ArrInitialization(int[] arr, int count) {
        int sum = 0;
        for (int i = 0; i < count; i++) {
            arr[i] = (int)(Math.random()*10);
            sum += arr[i];
        }
        return sum;
    }

    public static void ThreadInitialization(int countOfThread, int count, int[] arr, CounterThread[] counterThreads)
     {
        int[] sizeOfThreads = new int[countOfThread];
        for (int i = 0; i < countOfThread; i++) {
            sizeOfThreads[i] = count / countOfThread;
        }
        sizeOfThreads[sizeOfThreads.length - 1] += count % countOfThread;
        int mod = count % countOfThread;
        for (int i = 0; i < mod; i++) {
            sizeOfThreads[i]++;
            sizeOfThreads[sizeOfThreads.length - 1]--;
        }
        int start = 0;
        int finish;
        for (int i = 0; i < countOfThread; i++) {
            finish = start + sizeOfThreads[i];
//            System.out.println(start + " " + (finish - 1));
            counterThreads[i] = new CounterThread(start, finish - 1, arr);
            start += sizeOfThreads[i];
        }
    }

    public static void main(String[] args) throws InterruptedException {
        int count = (int)(Math.random() * 100);
        int[] arr = new int[count];
        int sum = ArrInitialization(arr, count);

        int countOfThread = (int)(Math.random() * 10);

        CounterThread[] counterThreads = new CounterThread[countOfThread];
        ThreadInitialization(countOfThread, count, arr, counterThreads);
        for (int i = 0; i < countOfThread; i++) {
            counterThreads[i].start();
        }

        for (int i = 0; i < countOfThread; i++) {
            System.out.println(counterThreads[i].sum);
        }
        System.out.println(mainSum + " " + sum);
    }
}
