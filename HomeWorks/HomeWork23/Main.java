package com.company;

import java.net.Socket;

public class Main {

    public static void main(String[] args) {
        HashMap<String, Integer> strings = new HashMap<>();
        String s = "";
        for (int i = 0; i < 100; i++) {
            int number = (int)( 1000 + Math.random() * 100);
            while (number > 0) {
                s += (char)(((int)'a') + number % 10);
                number /= 10;
            }
            strings.put(s, 1);
            System.out.println(s.hashCode() & 15);
            s = "";
        }

        System.out.println(strings.getCountOfCollision());

    }
}
