import java.util.Scanner;

public class program {

    public static void outMass(int[][] a, int n, int m) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.format("%4d", a[i][j]);
            }
            System.out.println();
        }
    }

    public static int[][] snake(int[][] a, int n, int m) {
        int j = 1;
        for (int k = 1; k <= Math.min(n, m) / 2 + 1; k++) {
            for (int i = k - 1; i < m - k + 1; i++) {
                a[k - 1][i] = j++;
            }
            if (k == Math.min(n, m) / 2 + 1 && n < m) {
                break;
            }
            for (int i = k; i < n - k + 1; i++) {
                a[i][m - k] = j++;
            }
            if (k == Math.min(n, m) / 2 + 1 && n > m) {
                break;
            }
            for (int i = m - k - 1; i >= k - 1; i--) {
                a[n - k][i] = j++;
            }
            for (int i = n - k - 1; i >= k; i--) {
                a[i][k - 1] = j++;
            }
        }
        return a;
    }

    public static int[][] sort(int[][] a, int n, int m) {
        int t = 0;
        for (int h = 0; h < n; h++) {
            for (int i = m - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (a[h][j] > a[h][j + 1]) {
                        t = a[h][j];
                        a[h][j] = a[h][j + 1];
                        a[h][j + 1] = t;
                    }
                }
            }
        }
        return a;
    }

    public static int[][] sum(int[][] a, int[][] b, int n, int m) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] += b[i][j];
            }
        }
        return a;
    }

    public static int[][] sub(int[][] a, int[][] b, int n, int m) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] -= b[i][j];
            }
        }
        return a;
    }

    public static int[][] mult(int[][] a, int[][] b, int[][] c, int n, int m, int k) {
        int s = 0;
        for (int i = 0; i < n; i++) {
            for (int h = 0; h < k; h++) {
                for (int j = 0; j < m; j++) {
                    s += a[i][j] * b[j][h];
                }
                c[i][h] = s;
                s = 0;
            }

        }
        return c;
    }

    public static int[][] zeroAboveDiagonal(int[][] a, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = 1 + (int) (Math.random() * 10);
            }
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                a[i][j] = 0;
            }
        }
        return a;
    }

    public static int[][] oneUnderDiagonal(int[][] a, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = 0;
            }
        }
        for (int i = 1; i < n; i++) {
            for (int j = n - i; j < n; j++) {
                a[i][j] = 1;
            }
        }
        return a;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите количетсво строчек");
        int n = sc.nextInt();
        System.out.println("Введите количетсво столбцов");
        int m = sc.nextInt();
        int a[][] = new int[n][m];
        int b[][] = new int[n][m];
        while (true) {
            System.out.println("1. SnakeMass.");
            System.out.println("2. SortMass.");
            System.out.println("3. SumMass.");
            System.out.println("4. SumMass.");
            System.out.println("5. MultMass.");
            System.out.println("6. Zero above Diagonal.");
            System.out.println("7. One under Diagonal.");
                int x = sc.nextInt();
            switch (x) {
                case 1: {
                    a = snake(a, n, m);
                    outMass(a, n, m);
                }
                break;
                case 2: {
                    for (int i = 0; i < n; i++) {
                        for (int j = 0; j < m; j++) {
                            a[i][j] = sc.nextInt();
                        }
                    }
                    a = sort(a, n, m);
                    outMass(a, n, m);
                }
                break;
                case 3: {
                    for (int i = 0; i < n; i++) {
                        for (int j = 0; j < m; j++) {
                            a[i][j] = sc.nextInt();
                        }
                    }
                    for (int i = 0; i < n; i++) {
                        for (int j = 0; j < m; j++) {
                            b[i][j] = sc.nextInt();
                        }
                    }
                    a = sum(a, b, n, m);
                    outMass(a, n, m);
                }
                break;
                case 4: {
                    for (int i = 0; i < n; i++) {
                        for (int j = 0; j < m; j++) {
                            a[i][j] = sc.nextInt();
                        }
                    }
                    for (int i = 0; i < n; i++) {
                        for (int j = 0; j < m; j++) {
                            b[i][j] = sc.nextInt();
                        }
                    }
                    a = sub(a, b, n, m);
                    outMass(a, n, m);
                }
                break;
                case 5: {
                    int k = sc.nextInt();
                    int[][] c = new int[n][k];
                    for (int i = 0; i < n; i++) {
                        for (int j = 0; j < m; j++) {
                            a[i][j] = sc.nextInt();
                        }
                    }
                    System.out.println("Введите второй массив");
                    for (int i = 0; i < m; i++) {
                        for (int j = 0; j < k; j++) {
                            b[i][j] = sc.nextInt();
                        }
                    }
                    c = mult(a, b, c, n, m, k);
                    outMass(c, n, k);
                }
                break;
                case 6: {
                    a = new int[n][n];
                    a = zeroAboveDiagonal(a, n);
                    outMass(a, n, n);
                }
                break;
                case 7: {
                    a = new int[n][n];
                    a = oneUnderDiagonal(a, n);
                    outMass(a, n, n);
                }
            }
        }
    }
}
