
public class Main {

    public static void main(String[] args) {
        MyLinkedList<Integer> myLinkedList = new MyLinkedList<>();
        myLinkedList.add(-1);
        myLinkedList.add(1);
        myLinkedList.add(2);
        myLinkedList.add(3);
        myLinkedList.add(4);
        MyStream<Integer> myStream = new MyStream<>(myLinkedList);
        myStream.filter(x -> x > 0).map(x -> (char)(x + 'a')).forEach(x -> System.out.println(x));

    }
}
