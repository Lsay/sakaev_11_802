public class Example {
    int[] a;
    public Example() {
        a = new int[]{1,1,1,2,2};
    }

    public Example filter1() {
        for (int i: a
             ) {
            if (i == 1) {
                i = 0;
            }
        }
        return this;
    }
    public Example filter2() {
        for (int i: a
        ) {
            if (i == 0) {
                i = 666;
            }
        }
        return this;
    }
}

