import java.util.Iterator;

public class MyLinkedList<T> implements Iterable<T> {
    private Node<T> head;

    public MyLinkedList<T> myStream() {
        return this;
    }

    private class MyLinkedListIterator implements Iterator<T> {
        Node<T> current = head;
        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            T result = current.value;
            current = current.next;
            return result;
        }
    }

    private class Node<T> {

        T value;
        Node<T> next;
        public Node(T value) {
            this.value = value;
        }

    }
    private boolean IsHead() {
        if (head == null) {
            return false;
        }
        return true;
    }

    public void add(T value) {
        if(!IsHead()) {
            head = new Node<>(value);
        } else {
            Node<T> node = head;
            while (node.next != null) {
                node = node.next;
            }
            node.next = new Node<>(value);
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new MyLinkedListIterator();
    }
}
