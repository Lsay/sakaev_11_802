class ch {
    public static void main(String[] args) {
        int a[] = {1, 22, 3, 4, 1};
        int koeff = 1; // koefficent dlya umnojeniya
        int num = 0;
        for (int i = a.length - 1; i >= 0; i--) {
            int temp = a[i];
            num += koeff * a[i];
            while (temp > 0) {
                temp /= 10;
                koeff *= 10;
            }
        }
        System.out.println(num);
    }
}
