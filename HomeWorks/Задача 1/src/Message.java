public class Message {
    User senderId;
    User receiverId;
    String text;
    boolean status;

    public Message(User senderId, User receiverId, String text, boolean status) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.text = text;
        this.status = status;
    }
}
