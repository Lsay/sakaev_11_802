public class Subscription {
    User subscriptionId;
    User subscriberId;

    public Subscription(User subscriptionId, User subscriberId) {
        this.subscriptionId = subscriptionId;
        this.subscriberId = subscriberId;
    }
}
