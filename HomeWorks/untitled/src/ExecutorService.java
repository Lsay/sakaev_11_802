import java.util.LinkedList;
import java.util.Queue;

public class ExecutorService {
    private final Object LOCK = new Object();
    private final ExecutorThread[] threads;
    private final Queue<Runnable> tasks;

    public ExecutorService(int maxThreadCount) {
        threads = new ExecutorThread[maxThreadCount];
        tasks = new LinkedList<>();
    }

    public void execute(Runnable task) {
        tasks.add(task);
        executeNextTask();
    }

    private int firstNullIndex() {
        for (int i = 0; i < threads.length; i++) {
            if (threads[i] == null) {
                return i;
            }
        }

        return -1;
    }

    private void executeNextTask() {
        synchronized (LOCK) {
            if (!tasks.isEmpty()) {
                int index = firstNullIndex();
                if (index != -1) {
                    threads[index] = new ExecutorThread(index, tasks.remove());
                    threads[index].start();
                }
            }
        }
    }

    private class ExecutorThread extends Thread {
        private int id;
        private Runnable task;

        public ExecutorThread(int id, Runnable task) {
            this.id = id;
            this.task = task;
        }

        @Override
        public void run() {
            task.run();

            synchronized (LOCK) {
                threads[id] = null;
                executeNextTask();
            }
        }
    }
}
