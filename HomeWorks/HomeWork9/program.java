    import java.util.Scanner;
    
    public class program {
    
        public static int[] readingMass(int m[], int n) {
            Scanner sc = new Scanner(System.in);
            m = new int[n];
            System.out.println("Введите сам массив, состоящий из " + n + " элементов");
            for (int i = 0; i < n; i++) {
                m[i] = sc.nextInt();
            }
            return m;
        }
    
        public static int[] simpleSort(int m[], int n) {
            int t;
            for (int i = n - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (m[j] > m[j + 1]) {
                        t = m[j];
                        m[j] = m[j + 1];
                        m[j + 1] = t;
                    }
                }
            }
            return m;
        }
    
        public static void sumOfOdd(int m[], int n) {
            int sum = 0;
            for (int i = 0; i < n; i++) {
                if (i % 2 == 1) {
                    sum += m[i];
                }
            }
            System.out.print("Полученная сумма: ");
            System.out.println(sum);
        }
    
        public static void reverce(int m[], int n) {
            for (int i = n - 1; i >= 0; i--) {
                System.out.print(m[i] + " ");
            }
            System.out.println();
        }
    
        public static int[] reverceMass(int m[], int n) {
            int t = 0;
            for (int i = 0; i < n / 2; i++) {
                t = m[i];
                m[i] = m[n - i - 1];
                m[n - i - 1] = t;
            }
            return m;
        }
    
        public static void localMax(int m[], int n) {
            int local = 0;
            for (int i = 1; i < n - 1; i++) {
                if ((m[i] > m[i - 1]) && (m[i] < m[i + 1])) {
                    local++;
                }
            }
            System.out.print("Количество локальных минимумов: ");
            System.out.println(local);
        }
    
        public static void massToInt(int m[], int n) {
            int chislo = 0;
            int k = 1;
            for (int i = n - 1; i >= 0; i--) {
                int t = m[i];
                chislo += m[i] * k;
                while (t > 0) {
                    t /= 10;
                    k *= 10;
                }
            }
            System.out.print("Полученное число: ");
            System.out.println(chislo);
        }
    
        public static void binSearch(int m[], int n) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Введите число, индекс которого нужно найти: ");
            int k = scanner.nextInt(); // chislo, index kotorogo nujno naiti
            int left = 0, right = 0; // granici
            right = m.length;
            int mid;
            while (right - left > 1) {
                mid = (left + right) / 2;
                if (k >= m[mid]) {
                    left = mid;
                } else {
                    right = mid;
                }
            }
            System.out.print("Индекс элемента: ");
            System.out.println(left);
        }
    
        public static void main(String[] args) {
            int a[];
            int x = 0;
            int lenght;
            a = new int[5];
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите размер массива");
            lenght = scanner.nextInt();
            a = readingMass(a, lenght);
            while (true) {
                System.out.println("Полученный массив:");
                for (int i = 0; i < lenght; i++) {
                    System.out.print(a[i] + " ");
                }
                System.out.println();
                System.out.println("Меню:");
                System.out.println("1. Отсортировать массив по возростанию.");
                System.out.println("2. Посчитать сумму элементов массива, стоящих на нечетных позициях.");
                System.out.println("3. Вывести массив обратном порядке.");
                System.out.println("4. Развернуть массив относительно центра.");
                System.out.println("5. Найти количество локальных максимумов.");
                System.out.println("6. Число из массива.");
                System.out.println("7. Бинарный поиск.");
                System.out.println("8. Выход.");
                x = scanner.nextInt();
                switch (x) {
                    case 1: {
                        a = simpleSort(a, lenght);
                    }
                    break;
                    case 2: {
                        sumOfOdd(a, lenght);
                    }
                    break;
                    case 3: {
                        reverce(a, lenght);
                    }
                    break;
                    case 4: {
                        a = reverceMass(a, lenght);
                    }
                    break;
                    case 5: {
                        localMax(a, lenght);
                    }
                    break;
                    case 6: {
                        massToInt(a, lenght);
                    }
                    break;
                    case 7: {
                        binSearch(a, lenght);
                    }
                    break;
                    case 8: {
                        System.exit(0);
                    }
                    default: {
                        System.out.println("Неизвестная команда! Введите заново");
                    }
                    break;
    
                }
            }
        }
    }

