import java.util.Scanner;

public class IntegerArrayList {
    private int[] elements;
    private int count;
    private boolean sorted;

    public IntegerArrayList(int[] elements, int count, boolean sorted) {
        this.setElements(elements);
        this.setCount(count);
        this.setSorted(sorted);
    }

    public void setElements(int[] elements) {
        this.elements = elements;
    }

    public int[] getElements() {
        return elements;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setSorted(boolean sorted) {
        this.sorted = sorted;
    }

    public boolean getSorted() {
        return sorted;
    }

    public void addElement() {
        this.count += 1;
        Scanner scanner = new Scanner(System.in);
        int elem = scanner.nextInt();
        newArray();
        this.elements[this.count - 1] = elem;
    }

    public void newArray() {
        int[] newArr = elements;
        elements = new int[count];
        for (int i = 0; i < count - 1; i++) {
            elements[i] = newArr[i];
        }
    }

    public void print() {
        for (int i = 0; i < elements.length; i++) {
            System.out.print(elements[i] + " ");
        }
        System.out.println();
    }

    public void sortElements() {
        int temp = 0;
        for (int i = 0; i < elements.length - 2; i++) {
            for (int j = i; j < elements.length - 1; j++) {
                if (elements[j] > elements[j + 1]) {
                    temp = elements[j];
                    elements[j] = elements[j + 1];
                    elements[j + 1] = temp;
                }
            }
        }
        sorted = true;
    }

    public void deleteElement(int n) {
        for (int i = n - 1; i < elements.length - 1; i++) {
            elements[i] = elements[i + 1];
        }
        int[] newArr = elements;
        elements = new int[count - 1];
        for (int i = 0; i < elements.length; i++) {
            elements[i] = newArr[i];
        }
        count--;
    }

    public void reverceElements() {
        int temp = 0;
        for (int i = 0; i < elements.length / 2; i++) {
            temp = elements[i];
            elements[i] = elements[elements.length - 1 - i];
            elements[elements.length - 1 - i] = temp;
        }
        sorted = false;
    }

    public int getIndexOfElement(int num) {
        int index = 0;
        if (sorted) {
            int left = 0, right = 0;
            right = elements.length;
            int mid;
            while (right - left > 1) {
                mid = (left + right) / 2;
                if (num >= elements[mid]) {
                    left = mid;
                } else {
                    right = mid;
                }
            }
            index = right;
        } else {
            for (int i = 0; i < elements.length; i++) {
                if (num == elements[i]) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    public void addElements(IntegerArrayList other1, IntegerArrayList other2) {
        int[] newArr = other2.elements;
        other2.elements = new int[other2.count + other1.count];
        for (int i = 0; i < other2.count; i++) {
            other2.elements[i] = newArr[i];
        }
        for (int i = 0; i < other1.count; i++) {
            other2.elements[other2.count + i] = other1.elements[i];
        }
    }

    public void printCount() {
        System.out.println(this.count);
    }
}
