public abstract class AbstractCivilCar extends Transport implements CivilTransport {
    public AbstractCivilCar(double oilAmount, String registrationNumber) {
        super(oilAmount, registrationNumber);
    }

    @Override
    public double getOilAmount() {
        return super.getOilAmount();
    }

    @Override
    public String getRegistrationNumber() {
        return super.getRegistrationNumber();
    }

    @Override
    public void drive() {

    }
}
