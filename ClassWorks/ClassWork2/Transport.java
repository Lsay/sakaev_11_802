public abstract class Transport {
    private double oilAmount;
    private String registrationNumber;

    public Transport(double oilAmount, String registrationNumber) {
        this.oilAmount = oilAmount;
        this.registrationNumber = registrationNumber;
    }

    public double getOilAmount() {
        return oilAmount;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public abstract void drive();
}
