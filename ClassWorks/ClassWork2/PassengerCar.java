public class PassengerCar extends Transport implements EditFuel{
    private boolean automaticTransmission;
    private String typeOfCar;

    public PassengerCar(double fuelTank, double capacity, boolean automaticTransmission, String typeOfCar) {
        super(fuelTank, capacity);
        this.automaticTransmission = automaticTransmission;
        this.typeOfCar = typeOfCar;
    }

    public boolean isAutomaticTransmission() {
        return automaticTransmission;
    }

    public void setAutomaticTransmission(boolean automaticTransmission) {
        this.automaticTransmission = automaticTransmission;
    }

    public String getTypeOfCar() {
        return typeOfCar;
    }

    public void setTypeOfCar(String typeOfCar) {
        this.typeOfCar = typeOfCar;
    }

    @Override
    public void reduce() {
        this.fuelTank -= 10;
    }

    @Override
    public void addFuel() {
        this.fuelTank += 20;
    }

    @Override
    public void printFuel() {
        System.out.println(this.fuelTank);
    }

}
