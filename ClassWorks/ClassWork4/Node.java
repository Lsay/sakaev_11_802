public class Node {
    private int value;
    private Node next;
    private boolean identifier;

    public Node(int value) {
        this.value = value;
        thisdentifier = false;
    }

    public int getValue() {
        return value;
    }

    public Node getNext() {
        return next;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public boolean getIdentifier() {
        return identifier;
    }

    public void setIdentifier(boolean identifier) {
        this.identifier = identifier;
    }
}

