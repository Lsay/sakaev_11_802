public class Main {

    public static void reverce(Node head) {
        Node temp = head;
        Node current = head;
        while (current.getNext() != null && !current.getIdentifier()) {
            current = current.getNext();
        }
        Node support = current;
        current.setValue(temp.getValue());
        temp.setValue(support.getValue());
    }

    public static void main(String[] args) {
        Node a = new Node(1);
        Node b = new Node(2);
        Node c = new Node(3);
        Node d = new Node(4);
        Node e = new Node(5);
        Node f = new Node(6);

        a.setNext(b);
        b.setNext(c);
        c.setNext(d);
        d.setNext(e);
        e.setNext(f);

        reverce(a);

        Node current = a;

        while (current.getNext() != null) {
            System.out.println(current.getValue() + "->");
            current = current.getNext();
        }
        System.out.println(current.getValue());
    }
}
