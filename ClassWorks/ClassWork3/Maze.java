public class Maze {

    private int width;
    private int height;
    private int[][] maze;
    public Maze(int width, int height) {
        this.maze = new int[width][height];
    }

    public void setWall(Cell cell) {
        this.maze[cell.getX()][cell.getY()] = -1;
    }

    public void setEmpty(Cell cell) {
        this.maze[cell.getX()][cell.getY()] = -2;
    }
}
