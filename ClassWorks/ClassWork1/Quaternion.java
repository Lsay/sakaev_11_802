public class Quaternion extends ComplexNumber{
    private double c;
    private double d;

    public Quaternion(double a, double b, double c, double d) {
        super(a, b);
        this.c = c;
        this.d = d;
    }

    public void calculateAbs() {
        this.abs = Math.sqrt(a * a + c * c + d * d);
    }

    public void printNumb() {
        System.out.println(a + " + " + b + " * i + " + c + " * j + " + d + " * k");
    }

    @Override
    public void printAbs() {
        super.printAbs();
    }
}
