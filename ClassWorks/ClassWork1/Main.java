public class Main {
    public static void main(String[] args) {
        Number number = new Number(1);
        ComplexNumber complexNumber = new ComplexNumber(1, 2);
        Quaternion quaternion = new Quaternion(1, 2, 3, 4);
        number.calculateAbs();
        number.printNumb();
        number.printAbs();
        complexNumber.calculateAbs();
        complexNumber.printNumb();
        complexNumber.printAbs();
        quaternion.calculateAbs();
        quaternion.printNumb();
        quaternion.printAbs();
    }
}
