import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

public class Main {

    public static void radixSort(ArrayList<Integer> dateList, int numberLength) {
        ArrayList[] tempArrayList = new ArrayList[10];
        int dec = 1;
        for (int i = 0; i < 10; i++) {
            tempArrayList[i] = new ArrayList();
        }
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < dateList.size(); j++) {
                int numberDate = dateList.get(j);
                tempArrayList[(numberDate/dec) % 10].add(numberDate);
            }
            int k = 0;
            int index = 0;
            while(index + 1 < dateList.size()) {
                for (int l = 0; l < tempArrayList[k].size(); l++) {
                    dateList.set(index, (Integer) tempArrayList[k].get(l));
                    index++;
                }
                tempArrayList[k] = new ArrayList();
                k++;
            }
            index = 0;
            k = 0;
            dec *= 10;
        }
    }



    public static void main(String[] args) throws IOException {
        Generator.generate();
        BufferedReader bufferedReader = new BufferedReader(new FileReader("out.txt"));
        ArrayList<Integer> dateList = new ArrayList<>();
        for (int i = 0; i < Generator.COUNT_OF_LINES; i++) {
            String s = bufferedReader.readLine();
            String[] dates = s.split("-");
            dateList.add(Integer.parseInt(dates[0])*10000 + Integer.parseInt(dates[1])*100 + Integer.parseInt(dates[2]));
        }
        radixSort(dateList, 5);
        bufferedReader.close();
        writeToFile(dateList);
    }

    private static void writeToFile(ArrayList<Integer> dateList) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("sortDates.txt"));
        for (int i = 0; i < Generator.COUNT_OF_LINES; i++) {
            int number = dateList.get(i);
            int day = number % 100;
            number /= 100;
            int month = number % 100;
            number /= 100;
            int year = number;
            bufferedWriter.write(year + "-" + month + "-" + day);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
    }
}
