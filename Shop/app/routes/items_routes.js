bodyParser = require('body-parser').json();

// описываем фунцию для обработки post-запроса на url /users
module.exports = function (app, fs) {
    app.post('/orders', bodyParser, function (request, response) {
        // вытаскиваю тело в формате JSON
        var body = request.body;
        console.log(body);
        // записываю его в файл
        fs.appendFile('dataOfOrders.txt', body.value + '\n',
            function (err) {
                if (err) throw err;
                console.log('Saved!');
                response.redirect("/html/index.html");
            });
    });
    app.get('/orders', function (request, response) {
        fs.readFile('dataOfOrders.txt', 'utf-8', function(err, data) {
            var lines = data.split('\n');

            var result = [];
            for (var i = 0; i < lines.length; i++) {
                result.push({'value' : lines[i].split(' ')[0]});
            }
            response.setHeader('Content-Type', 'application/json');
            response.send(JSON.stringify(result));
        });
    });
};